import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import {CloudCircle, Dns} from "@material-ui/icons";
import {Button, Divider, Hidden, Link, ListItemIcon, ListItemText} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'flex',
        alignItems: 'center',
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            alignItems: 'center',
        },
    },
    sectionDesktop2: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            alignItems: 'center',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
}));

const menuItemsMain = () => {
    return [
        {
            title: "Simulateur",
            icon: <Dns/>,
            path: "/simulateur",
        },
    ]
}

const menuItemsSecondary = () => {
    return [
        {
            title: "Panel",
            icon: <AccountCircle/>,
            path: "https://panel.minekloud.com/",
        },
    ]

}

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu{...props}/>
));

export default function MenuBar() {
    const classes = useStyles();
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMobileMenuOpen = (event) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const renderMobileMenu = (
        <StyledMenu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            keepMounted
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
            onClick={handleMobileMenuClose}
        >
            {menuItemsMain().map(value => (
                <NavLink to={value.path}>
                    <MenuItem>
                        <ListItemIcon>
                            {value.icon}
                        </ListItemIcon>
                        <ListItemText primary={value.title}/>
                    </MenuItem>
                </NavLink>
            ))}
            <Hidden smUp>
                <Divider/>
                {menuItemsSecondary().map(value => (
                    <Link href={value.path} color="inherit">
                        <MenuItem>
                            <ListItemIcon>
                                {value.icon}
                            </ListItemIcon>
                            <ListItemText primary={value.title}/>
                        </MenuItem>
                    </Link>
                ))}
            </Hidden>
        </StyledMenu>
    );

    return (
        <div className={classes.grow}>
            <AppBar position="static">
                <Toolbar>
                    <NavLink to="/" className={classes.title} style={{marginRight: 50}}>
                        <CloudCircle className={classes.menuButton}/>
                        <Typography variant="h6" noWrap>
                            Minekloud
                        </Typography>
                    </NavLink>
                    <div className={classes.sectionDesktop}>
                        {menuItemsMain().map(value => (
                            <NavLink to={value.path}>
                                <Button startIcon={value.icon} className={classes.menuButton} size="large"
                                        color="inherit">
                                    {value.title}
                                </Button>
                            </NavLink>
                        ))}
                    </div>
                    <div className={classes.grow}/>
                    <div className={classes.sectionDesktop2}>
                        {menuItemsSecondary().map(value => (
                            <Link href={value.path} color="inherit">
                                <Button startIcon={value.icon} className={classes.menuButton} size="large"
                                        color="inherit">
                                    {value.title}
                                </Button>
                            </Link>
                        ))}
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton onClick={handleMobileMenuOpen} color="inherit">
                            <MoreIcon/>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
        </div>
    );
}