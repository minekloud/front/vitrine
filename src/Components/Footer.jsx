import {NavLink} from "react-router-dom";
import {Box, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import {Copyright} from "@material-ui/icons";
import React from "react";

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
    copyright: {
        marginRight: theme.spacing(1),
    }
}));

const footers = [
    {
        title: 'Company',
        description: [{
            name: 'Team',
            path: 'https://gitlab.com/groups/cpelyon/5irc-minekloud/-/group_members'
        }, {name: 'Contact us', path: 'mailto:contact@minekloud.com'}, {
            name: 'Locations',
            path: 'https://goo.gl/maps/fy1GSPcPsXwQPVAm9'
        }],
    },
    {
        title: 'Legal',
        description: [{ name: 'Privacy policy', path: '/privacy' }, { name: 'Terms of use', path: '/terms' }]
    },
];

export default function Footer() {
    const classes = useStyles();
    return (
        <Container maxWidth="md" component="footer" className={classes.footer}>
            <Grid container spacing={4} justify="space-evenly">
                {footers.map((footer) => (
                    <Grid item xs={6} sm={3} key={footer.title}>
                        <Typography variant="h6" color="textPrimary" gutterBottom>
                            {footer.title}
                        </Typography>
                        <ul>
                            {footer.description.map((item) => (
                                <li key={item.name}>
                                    <FooterLink path={item.path}>
                                        <Typography variant="subtitle1" color="textSecondary">
                                            {item.name}
                                        </Typography>
                                    </FooterLink>
                                </li>
                            ))}
                        </ul>
                    </Grid>
                ))}
            </Grid>
            <Box mt={5} display="flex" alignItems="center" flexDirection="row" spacing={2}>
                <Copyright className={classes.copyright}/>
                <Typography>
                    Minekloud 2021
                </Typography>
            </Box>
        </Container>
    )
}

function FooterLink(props) {
    if (props.path.startsWith("/")) {
        return (
            <NavLink to={props.path}>
                {props.children}
            </NavLink>
        )
    } else {
        return (
            <Link href={props.path}>
                {props.children}
            </Link>
        )
    }
}