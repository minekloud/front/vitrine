FROM nginx:1.19.6-alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf
WORKDIR /usr/share/nginx/html
COPY build .
EXPOSE 80
